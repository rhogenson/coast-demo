#!/usr/bin/env nix-shell
#!nix-shell -i python3 -p "python3.withPackages (pkgs: with pkgs; [ gdal numpy ])"
import os
import random
import sys

from osgeo import gdal
import numpy


def land_file():
    try:
        d = os.path.dirname(os.path.realpath(__file__))
    except NameError:
        d = os.getcwd()
    return d+'/ne_10m_land.shp'


def make_map(xmin, ymin, res=0.05, xsize=100, ysize=50):
    gdal.Rasterize('/vsimem/map.tiff', land_file(), xRes=res, yRes=res, outputBounds=[xmin, ymin, res*xsize+xmin, res*ysize+ymin], outputType=gdal.GDT_Byte)
    raster_map = gdal.OpenEx('/vsimem/map.tiff')
    return raster_map.ReadAsArray()


def print_map(m):
    for row in m:
        for x in row:
            if x:
                print('#', end='')
            else:
                print(' ', end='')
        print()


def land_percent(m):
    return 100 * numpy.count_nonzero(m) / (m.shape[0]*m.shape[1])


def main():
    if len(sys.argv) != 1 and len(sys.argv) != 4:
        print("Usage: coast-demo.py LATITUDE LONGITUDE RESOLUTION\nor call with no arguments to generate a random map at 0.05 resolution.")
        return

    if len(sys.argv) == 4:
        print_map(make_map(float(sys.argv[2]), float(sys.argv[1]), float(sys.argv[3])))
        return

    while True:
        xsize = 100
        ysize = 50
        res = 0.05
        xrange = 180 - xsize*res
        yrange = 360 - ysize*res
        ymin = random.random()*xrange-90
        xmin = random.random()*yrange-180
        m = make_map(xmin, ymin, res, xsize, ysize)
        if 40 < land_percent(m) < 90:
            print_map(m)
            break


main()
